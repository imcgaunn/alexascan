import util
import time
import sys
from bs4 import BeautifulSoup

class ScrapeRes:
    """ class which encapsulates result of a scraping operation """
    def __init__(self, url, dur, hdr, words):
        self.url = url
        self.dur = dur 
        self.hdr = hdr  
        self.words = words

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

class ScrapeReport:
    """ class used for analyzing ScrapeRes objects. Given a set of
        ScrapeRes objects, generates a report based on their aggregated
        properties """
    def __init__(self, results, err):
        self.aggregate(results)
        self.errors = err

    def aggregate(self, results):
        self.num_results = len(results)
        # the sum of all 'words' fields in self.results 
        self.total_words = reduce(lambda a, b: a + b,
                                  map(lambda x: x.words,
                                      results))

        # determine rankings by word count
        self.wordcount_ranked = sorted(results, 
                                       key=lambda x: x.words,
                                       reverse=True)

        # the sum of all 'ts' fields in self.results
        self.total_time = reduce(lambda a, b: a + b,
                                 map(lambda x: x.dur,
                                     results))

        # determine rankings by scan duration
        self.duration_ranked = sorted(results,
                                      key=lambda x: x.dur,
                                      reverse=True)

        # find top 20 HTTP headers used 
        res_hdrs = map(lambda x: x.hdr, results) 
        self.hdr_hist = {} 
        for h in res_hdrs:
            for k in h.keys():
                # inc if there's already a histogram entry for k,
                # otherwise itialize to 1.
                hist_val = self.hdr_hist[k] + 1 if k in self.hdr_hist else 1
                self.hdr_hist[k] = hist_val

        # res. of map is a list of tuples in form (hdr, freq). 
        # sorted by freq in decreasing order.
        self.hdrs_sorted = sorted(self.hdr_hist,
                             key=self.hdr_hist.get,
                             reverse=True)
        self.top_headers = self.hdrs_sorted[:20] # extract only top 20 (or fewer)
    
    def print_report(self):
        """ Prints full report of site analysis. This report includes the following:
            per-site: word count and how it ranks to other sites included in report
            per-report: AVG word count of sites included in report,
                        Top 20 headers and usage percentage of each in sites included,
                        Duration of scan. """

        print "==Averages:=="
        print "top {} HTTP response headers:".format(len(self.top_headers))
        for i, hdr in enumerate(self.top_headers):
            print "%d.) %s" % ((i + 1), hdr)
            freq = self.hdr_hist[hdr] / float(self.num_results)
            print "frequency: %f" % freq 

        print "total words: {}".format(self.total_words)
        print "average words: {}".format(self.total_words / self.num_results)
        print "total duration of scan: {}".format(self.total_time)
        print ""
        
        print "==Ranked Results:=="
        print "by word count:"
        for i, res in enumerate(self.wordcount_ranked):
            print "%d.)" % (i + 1)
            print "\tsite: {}".format(res.url)
            print "\tword count: {}".format(res.words)
        print ""

        print "by scan duration:"
        for i, res in enumerate(self.duration_ranked):
            print "%d.)" % (i + 1)
            print "\tsite: {}".format(res.url)
            print "\tscan duration: {}".format(res.dur)
        print ""

        if self.errors:
            print "==Failed To Scan:=="
            for i, err in enumerate(self.errors):
                print "%d.)" % (i + 1)
                print "site: {}".format(err['url'])
                print "reason: {}".format(err['reason'])

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

def _scrape(page):
    """ function which takes HTML repr. of webpage and extracts total
        number of words """
    parser = BeautifulSoup(page, 'html.parser')
    text = parser.get_text()
    words = text.split(' ') # defining words as tokens separated by spaces.
    return len(words)

def scrape(url):
    """ harness for _scrape(html) which gets a timestamp
        before and after running _scrape(html) and returns a ScrapeRes. """

    t1  = time.time()

    res = util.download_and_report(url)
    html = res['html']
    hdrs = res['headers']
    words = _scrape(html)

    t2 = time.time()
    total_time = t2 - t1

    return ScrapeRes(url, total_time, hdrs, words)

def main():
    scrape_urls = util.get_alexa_top_sites(100)
    results = []
    err = []
    for url in scrape_urls:
        try:
            print "scraping {}".format(url)
            results.append(scrape(url))
        except Exception as e:
            err.append({"url": url, "reason": reason})
    if results:
        report = ScrapeReport(results, err)
        report.print_report()

if __name__ == '__main__':
    sys.exit(main())
