# To Run This Program
python alexa_spider.py

# Dependencies
There is only one dependency outside of the python standard library,
which is BeautifulSoup 4. This can be installed with `sudo pip install bs4`
on Unix-like systems with pip installed.

This program was developed using Python 2.7.13 on Mac OS X 10.12.3 

