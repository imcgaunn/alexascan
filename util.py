import urllib2
from zipfile import ZipFile

def download_and_report(url):
    """ makes a GET request for URL and returns response body and headers. """
    try:
        u = urllib2.urlopen(url)
        return {'html': u.read(), 'headers': u.info().dict}
    except urllib2.URLError:
        raise

def get_alexa_top_sites(n):
    """ retrieves top n alexa sites globally. """

    # found URL here: https://gist.github.com/chilts/7229605
    u = urllib2.urlopen('http://s3.amazonaws.com/alexa-static/top-1m.csv.zip')
    with open('top-1m.csv.zip', 'w') as f:
        f.write(u.read())
    with ZipFile('top-1m.csv.zip', 'r') as z:
        with z.open('top-1m.csv') as csv:
            lines = csv.readlines()
            # file is formatted linewise like: <rank, site(no scheme)>
            top_n = lines[:n]
            # return just the site portion for each, with an HTTP scheme prepended
            return map(lambda x: 'http://www.'+x.split(',')[1], top_n)
